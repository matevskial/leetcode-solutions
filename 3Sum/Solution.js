// https://leetcode.com/problems/3sum/

function binarySearch(start, arr, x) { 
   
    let end=arr.length-1; 
          
    // Iterate while start not meets end 
    while (start<=end){ 
  
        // Find the mid index 
        let mid=Math.floor((start + end)/2); 
   
        // If element is present at mid, return True 
        if (arr[mid]===x) return true; 
  
        // Else look in left or right half accordingly 
        else if (arr[mid] < x)  
             start = mid + 1; 
        else
             end = mid - 1; 
    } 
   
    return false; 
} 

/**
 * @param {number[]} nums
 * @return {number[][]}
 */
const threeSum = nums => {
    nums.sort((a, b) => a - b);
    
    let result = [];
    
    for(let i = 0; i < nums.length; i++) {
        let iNum = nums[i];
        if(i > 0 && iNum === nums[i-1]) continue;
        
        for(let j = i + 1; j < nums.length - 1; j++) {
            let jNum = nums[j];
            if(j > i+1 && jNum === nums[j-1]) continue;
            
            let complement = 0 - (iNum + jNum);
            if(binarySearch(j+1, nums, complement))
                result.push([iNum, jNum, complement]);
        }
    }
    
    return result;
};

console.log(threeSum([2, 0, 1, -1]));

module.exports = threeSum;
