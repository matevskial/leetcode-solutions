class Solution {
    
    private boolean binarySearch(int l, int r, int target, int[] nums) {
        if(l > r)
            return false;
        
        while(l <= r) {
            int mid = l + (r - l) / 2;
            
            if(nums[mid] == target)
                return true;
            else if(nums[mid] > target)
                r = mid - 1;
            else
                l = mid + 1;
        }
        
        return false;
    }
    
    public List<List<Integer>> threeSum(int[] nums) {
        // sort the numbers
        // for each unique pair with indexes i and j
            // if there exists a complement with index > j
                // add triplet to result list
            
        Arrays.sort(nums);
        
        List<List<Integer>> triplets = new ArrayList<>();
        for(int i = 0; i < nums.length; i++) {
            int n = nums[i];
            
            if(i > 0 && n == nums[i - 1])
                continue;
            
            for(int j = i + 1; j < nums.length; j++){
                int m = nums[j];
                
                if(j > i + 1 && m == nums[j - 1])
                    continue;
                
                int complement = 0 - (n + m);
                List<Integer> triplet = new ArrayList<>(Arrays.asList(n, m, complement));

                
                if(binarySearch(j + 1, nums.length - 1, complement, nums))
                    triplets.add(triplet);
            }
        }
        
        return triplets;
    }
}
