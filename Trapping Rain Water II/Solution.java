import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Deque;
import java.util.HashSet;

class Solution {
	int n, m;
	int[][] heightMap;
	Set<Integer> processed;
	int[][] result;

	int[] dirI = {-1, 0, 1, 0};
	int[] dirJ = {0, 1, 0, -1};

	public int trapRainWater(int[][] heightMap) {
		this.heightMap = heightMap;
		n = heightMap.length;
		m = heightMap[0].length;
		result = new int[n][m];
		processed = new HashSet<>();
		Map<Integer, List<Integer>> map = new TreeMap<>();

		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) {
				int val = heightMap[i][j];

				result[i][j] = val;

				int uniq = m * i + j;
				if (!map.containsKey(val)) {
					map.put(val, new ArrayList<>());
				}

				map.get(val).add(uniq);

			}
		}

		for (Integer v : map.keySet()) {
			List<Integer> indexes = map.get(v);
			for (Integer index : indexes) {
				int ci = index / m;
				int cj = index - (ci * m);

				if (!processed.contains(index)) {
					calculateVolume(ci, cj);
				}
			}
		}

		int total = 0;
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) {
				total += (result[i][j] - heightMap[i][j]);
			}
		}

		return total;
	}

	void calculateVolume(int si, int sj) {
		Deque<Integer> st = new ArrayDeque<>();
		Set<Integer> visitedNodes = new HashSet<>();

		boolean outOfBound = false;
		int minBound = Integer.MAX_VALUE;

		st.push(m * si + sj);
		visitedNodes.add(m * si + sj);
		processed.add(m * si + sj);

		while (!st.isEmpty()) {
			int c = st.pop();
			int ci = c / m;
			int cj = c - (ci * m);

			for (int d = 0; d < 4; d++) {
				int ni = ci + dirI[d];
				int nj = cj + dirJ[d];

				int uniq = ni * m + nj;

				// out of bound
				if (ni < 0 || nj < 0 || ni >= n || nj >= m) {
					outOfBound = true;
					continue;
				}

				if (result[ni][nj] == result[si][sj] && !visitedNodes.contains(uniq)) {
					visitedNodes.add(uniq);
					processed.add(uniq);
					st.push(uniq);
				} else if (result[ni][nj] > result[si][sj]) {
					minBound = Math.min(minBound, result[ni][nj]);
				} else if (result[ni][nj] < result[si][sj] && result[ni][nj] == heightMap[ni][nj]) {
					outOfBound = true;
				}
			}
		}

		if (outOfBound || minBound == Integer.MAX_VALUE) {
			minBound = heightMap[si][sj];
		}

		for (Integer v : visitedNodes) {
			int vi = v / m;
			int vj = v - (vi * m);

			result[vi][vj] += (minBound - result[vi][vj]);
		}
	}

	public static void main(String[] args) {
		int[][] heightMap =
				new int[][] {{1, 4, 3, 1, 3, 2}, {3, 2, 1, 3, 2, 4}, {2, 3, 3, 2, 3, 1}};

		int[][] heightMap1 = new int[][] {{5, 5, 5, 1}, {5, 1, 1, 5}, {5, 1, 5, 5}, {5, 2, 5, 8}};

		int[][] heightMap2 =
				new int[][] {{1, 2, 2, 2, 1}, {2, 1, 1, 1, 2}, {2, 1, 1, 1, 2}, {1, 2, 2, 2, 1}};

		int[][] heightMap3 = new int[][] {{4, 4, 4, 4}, {4, 1, 1, 4}, {4, 3, 4, 4}};

		int[][] heightMap4 =
				new int[][] {{4, 4, 4, 4, 4}, {4, 2, 1, 1, 2}, {2, 1, 1, 1, 2}, {2, 2, 2, 2, 2}};

		int[][] heightMap5 =
				new int[][] {{4, 4, 4, 4, 4}, {4, 1, 1, 2, 4}, {2, 1, 1, 2, 4}, {2, 2, 2, 2, 2}};

		int[][] heightMap6 = new int[][] {{12, 13, 1, 12}, {13, 4, 13, 12}, {13, 8, 10, 12},
				{12, 13, 12, 12}, {13, 13, 13, 13}};

		System.out.println(new Solution().trapRainWater(heightMap)); // 4
		System.out.println(new Solution().trapRainWater(heightMap1)); // 3
		System.out.println(new Solution().trapRainWater(heightMap2)); // 6
		System.out.println(new Solution().trapRainWater(heightMap3)); // 4
		System.out.println(new Solution().trapRainWater(heightMap4)); // 5
		System.out.println(new Solution().trapRainWater(heightMap5)); // 4
		System.out.println(new Solution().trapRainWater(heightMap6)); // 14

	}
}
