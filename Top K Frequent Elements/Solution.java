class Solution {

    public int[] topKFrequent(int[] nums, int k) {
        Map<Integer, Integer> count = new HashMap<>();
        for(int i = 0; i < nums.length; i++) {
            count.put(nums[i], count.getOrDefault(nums[i], 0) + 1);
        }
        
        PriorityQueue<Integer> p = new PriorityQueue<>((a, b) -> count.get(a) - count.get(b));
        
        for(Integer e : count.keySet()) {
            p.add(e);
            
            if(p.size() > k)
                p.poll();
        }
        
        int[] result = new int[k];
        for(int i = 0; i < k; i++) {
            result[i] = p.poll();
        }
        
        return result;
    }
}
