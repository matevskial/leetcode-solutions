// https://leetcode.com/problems/symmetric-tree/

public class Solution {
	private boolean isSymmetric(TreeNode A, TreeNode B) {
		if (A == null && B == null)
			return true;
		if (A == null || B == null)
			return false;
		return (A.val == B.val && isSymmetric(A.left, B.right) && isSymmetric(A.right, B.left));
	}

	public boolean isSymmetric(TreeNode root) {
		return isSymmetric(root, root);
	}

	private static TreeNode makeTree(Integer[] arr, int rootIndex) {
		if (rootIndex >= arr.length || arr[rootIndex] == null) {
			return null;
		}

		TreeNode root = new TreeNode(arr[rootIndex]);
		root.left = makeTree(arr, 2 * rootIndex + 1);
		root.right = makeTree(arr, 2 * rootIndex + 2);

		return root;
	}

	public static void main(String[] args) {
		TreeNode root = makeTree(new Integer[] {1, 2, 2, 3, 4, 4, 3}, 0);
		System.out.println(new Solution().isSymmetric(root));

		root = makeTree(new Integer[] {1, 2, 2, null, 3, null, 3}, 0);
		System.out.println(new Solution().isSymmetric(root));
	}
}


/* Definition for a binary tree node. */
class TreeNode {
	int val;
	TreeNode left;
	TreeNode right;

	TreeNode(int x) {
		val = x;
	}
}

