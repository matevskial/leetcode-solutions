class Solution {
    
    private boolean isPalindrome(String s) {
        int mid = s.length();
        for(int i = 0; i < mid; i++){
            if(s.charAt(i) != s.charAt(s.length() - i - 1)) {
                return false;
            }
        }
        return true;
    }
    
    // this method assumes letters is always palindromic
    private int getMinStepsForPalindrome(List<Character> letters, List<Integer> count) {
        int mid = count.size() / 2;
        int result = 0;
        for(int i = 0; i < mid; i++) {
            result += Math.abs(count.get(i) - count.get(count.size() - i - 1));
        }
        return result + 1;
    }
    
	private int solve(String s) {
        if(isPalindrome(s)) {
            return 1;
        }
        return 2;
	}
	
	private int solve1(String s) {
		 List<Integer> count = new ArrayList<>();
		 List<Character> letters = new ArrayList<>();
        
		 for(int i = 0; i < s.length(); i++) {
			 if(letters.size() == 0 
				|| letters.get(letters.size() - 1) != s.charAt(i)) {
				 letters.add(s.charAt(i));
				 count.add(1);
			 } else {
				 int tmp = count.get(count.size() - 1) + 1;
				 count.set(count.size() - 1, tmp);
			 }
		 }
        
		 // sequences of 2 alternating characters are palindrome if the size is odd number
		 if(letters.size() % 2 == 1) {
			 return getMinStepsForPalindrome(letters, count);
		 } else {
			 List<Character> removedFirstLetters = IntStream.range(1, letters.size())
				 .mapToObj(letters::get).collect(Collectors.toList());
			 List<Integer> removedFirstCount = IntStream.range(1, count.size())
				 .mapToObj(count::get).collect(Collectors.toList());
            
			 List<Character> removedLastLetters = IntStream.range(0, letters.size() - 1) 
				 .mapToObj(letters::get).collect(Collectors.toList());
			 List<Integer> removedLastCount = IntStream.range(0, count.size() - 1)
				 .mapToObj(count::get).collect(Collectors.toList());
            
			 return 1 + Math.min(getMinStepsForPalindrome(removedFirstLetters, removedFirstCount),
					 getMinStepsForPalindrome(removedLastLetters, removedLastCount));
		}
	}

    public int removePalindromeSub(String s) {
       return solve(s); 
        
    }
}
