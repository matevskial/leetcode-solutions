// https://leetcode.com/problems/container-with-most-water/

/**
 * @param {number[]} height
 * @return {number}
 */
function maxArea(height) {
    const indexes = {};
    
    let h = height.map((a, i) => [a, i]);
    h.sort((a, b) => a[0] - b[0]);
    
    let currMax = -Infinity, currMin = Infinity;
    let result = 0;
    for(let i = h.length - 1; i >= 0; i--) {
        let pair = h[i];
        if(currMax !== -Infinity)
            result = Math.max(result, pair[0] * Math.abs(currMax - pair[1]));
        if(currMin !== Infinity)
            result = Math.max(result, pair[0] * Math.abs(currMin - pair[1]));
        
        currMax = Math.max(currMax, pair[1]);
        currMin = Math.min(currMin, pair[1]);
    }
    
    return result;
}

function main() {
    let heights = [1,8,6,2,5,4,8,3,7];
    console.log(maxArea(heights));
}

main()

module.exports = maxArea;
