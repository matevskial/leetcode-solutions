// https://leetcode.com/problems/assign-cookies/

import java.util.Arrays;

public class Solution {
	public int findContentChildren(int[] g, int[] s) {
		Arrays.sort(g);
		Arrays.sort(s);

		int result = 0;
		int j = 0;
		for (int i = 0; i < g.length; i++) {
			boolean assigned = false;
			while (j < s.length) {
				if (s[j] >= g[i]) {
					assigned = true;
					j++;
					break;
				}
				j++;
			}

			if (assigned) {
				result++;
			} else {
				break;
			}
		}

		return result;
	}

	public static void main(String[] args) {
        int[] g = new int[]{1, 2};
        int[] s = new int[]{1, 2, 3]};
		System.out.println(new Solution().findContentChildren(g, s));
	}
}
