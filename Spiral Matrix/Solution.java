class Solution {
    public List<Integer> spiralOrder(int[][] matrix) {
        List<Integer> result = new ArrayList<>();
        
        int n = matrix.length;
        int m = matrix[0].length;
        int steps = n * m;
        int row = 0, col = 0, level = 0, dir = 0;
        for(int s = 0; s < steps; s++) {
            result.add(matrix[row][col]);
            
            if(dir == 0 && col == m - level - 1) {
                dir++;
            } else if(dir == 1 && row == n - level - 1) {
                dir++;
            } else if(dir == 2 && col == level) {
                dir++;
            } else if(dir == 3 && row == level + 1) {
                dir = 0;
                level++;
            }
            
            if(dir == 0)
                col++;
            else if(dir == 1)
                row++;
            else if(dir == 2)
                col--;
            else if(dir == 3)
                row--;
        }
        
        return result;
    }
}
