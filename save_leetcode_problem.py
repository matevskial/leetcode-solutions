#!/usr/bin/env python3

import requests
import os
from bs4 import BeautifulSoup
import sys
import re

from PyQt5.QtWebEngineWidgets import QWebEnginePage 
from PyQt5.QtWidgets import QApplication 
from PyQt5.QtCore import QUrl 

class Page(QWebEnginePage): 
    def __init__(self, url): 
        self.app = QApplication(sys.argv) 
        QWebEnginePage.__init__(self) 
        self.html = '' 
        self.loadFinished.connect(self._on_load_finished) 
        self.load(QUrl(url)) 
        self.app.exec_() 
  
    def _on_load_finished(self): 
        self.html = self.toHtml(self.Callable) 
        print('Load finished') 
  
    def Callable(self, html_str): 
        self.html = html_str 
        self.app.quit() 

if __name__ == "__main__":
    problem_url = sys.argv[1]
    problem_categories = ""
    for c in range(2, len(sys.argv)):
        problem_categories += sys.argv[c]
        if c != 2:
            categories += ", "
    
    page = Page(problem_url).html

    soup = BeautifulSoup(page, features='html.parser') # explicitly set the HTML parser

    problem_title = soup.select_one("div[data-cy=\"question-title\"]").text
    reg = re.compile(r'^\d+.\s+')
    problem_title = reg.sub("", problem_title)

    problem_difficulty = soup.select_one("div[diff]").text

    problem_statement = soup.select_one("div.question-content__JfgR").get_text()

    os.mkdir(problem_title)

    with open(problem_title + "/" + problem_title + ".txt", 'w') as f:
        f.write(problem_title + "\n")
        f.write(problem_categories + " - " + problem_difficulty + "\n")
        f.write(problem_url + "\n")
        f.write("---------------\n" + "---------------\n\n")
        f.write("Problem Statement\n")
        f.write("-----------------\n\n")
        f.write(problem_statement)
