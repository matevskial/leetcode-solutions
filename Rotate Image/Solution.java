class Solution {
    public void rotate(int[][] matrix) {
        for(int l = 0; l < matrix.length / 2; l++) {
            
            for(int i = 0; i < matrix.length - 2*l - 1; i++) {
                int a = matrix[l][l+i];
                int b = matrix[l+i][matrix.length - l - 1];
                int c = matrix[matrix.length - l - 1][matrix.length - l - 1 - i];
                int d = matrix[matrix.length - l - 1 - i][l];
                
                matrix[l][l+i] = d;
                matrix[l+i][matrix.length - l - 1] = a;
                matrix[matrix.length - l - 1][matrix.length - l - 1 - i] = b;
                matrix[matrix.length - l - 1 - i][l] = c;
            }
        }
    }
}
