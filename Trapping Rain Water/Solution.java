// https://leetcode.com/problems/trapping-rain-water/

class Solution {
	public int trap(int[] A) {
		int[] maxForward = new int[A.length];
		int[] maxBackward = new int[A.length];

		for (int i = 0; i < A.length; i++) {
			if (i == 0) {
				maxForward[i] = 0;
			} else {
				maxForward[i] = Math.max(maxForward[i - 1], A[i - 1]);
			}
		}

		for (int i = A.length - 1; i >= 0; i--) {
			if (i == A.length - 1) {
				maxBackward[i] = 0;
			} else {
				maxBackward[i] = Math.max(maxBackward[i + 1], A[i + 1]);
			}
		}

		int res = 0;
		for (int i = 0; i < A.length; i++) {
			if (A[i] <= maxForward[i] && A[i] <= maxBackward[i]) {
				res += Math.abs(A[i] - Math.min(maxForward[i], maxBackward[i]));
			}
		}

		return res;
	}

	public static void main(String[] args) {
		int[] heights = {0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1};
		System.out.println(new Solution().trap(heights));
	}
}
