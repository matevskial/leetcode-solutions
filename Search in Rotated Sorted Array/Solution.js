// https://leetcode.com/problems/search-in-rotated-sorted-array/

/** Finds the index of the smallest number in the rotated sorted array
 * @param {number[]} nums
 * @return {number}
 */
function findMinsIndex(nums) {
    let start = 0, end = nums.length - 1;
    while(start <= end) {
        let mid = Math.floor((end + start) / 2);
        
        if(end - start <= 1)
            return (nums[start] < nums[end]) ? start : end;
        else if(nums[mid-1] > nums[mid] && nums[mid+1] > nums[mid])
            return mid;
        else if(nums[end] > nums[mid])
            end = mid - 1;
        else
            start = mid + 1;
    }
    
    return NaN;
}

function binarySearch(start, end, nums, target) {
    while(start <= end) {
        let mid = Math.floor((start + end) / 2);
        
        if(nums[mid] == target)
            return mid;
        else if(nums[mid] < target)
            start = mid + 1;
        else
            end = mid - 1;
    }
    
    return -1;
}
/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number}
 */
function search(nums, target) {
    let minsIndex = findMinsIndex(nums);
    
    if(nums[minsIndex] == target) return minsIndex;
    
    let foundOnLeft = binarySearch(0, minsIndex - 1, nums, target);
    let foundOnRight = binarySearch(minsIndex + 1, nums.length - 1, nums, target);
    
    return (foundOnLeft == -1) ? foundOnRight : foundOnLeft;
}

function main() {
	let nums = [4,5,6,7,0,1,2];
	console.log(search(nums, 0));
}

main();

module.exports = search;
