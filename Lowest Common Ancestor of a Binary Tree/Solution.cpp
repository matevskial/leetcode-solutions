#include <bits/stdc++.h>

// https://leetcode.com/problems/lowest-common-ancestor-of-a-binary-tree/

using namespace std;

/*
 Definition for a binary tree node.
 */
struct TreeNode {
	int val;
	TreeNode *left;
	TreeNode *right;
	TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class Solution {
public:
    TreeNode* lca = NULL;
    
    int traverse(TreeNode *root, TreeNode* p, TreeNode* q) {
        if(root == NULL)
            return -1;
        
        int leftRes = traverse(root->left, p, q);
        
        if(leftRes == 0)
            return 0;
        
        int rightRes = traverse(root->right, p, q);
        if(rightRes == 0)
            return 0;
        
        int rootRes = -1;
        
        if(root == p)
            rootRes = 1;
        if(root == q)
            rootRes = 2;
        
        if( (leftRes == 1 && rightRes == 2) || (leftRes == 2 && rightRes == 1) ) {
            lca = root;
            return 0;
        } else if(rootRes == 1 && (leftRes == 2 || rightRes == 2)) {
            lca = root;
            return 0;
        } else if(rootRes == 2 && (leftRes == 1 || rightRes == 1)) {
            lca = root;
            return 0;
        } else {
            if(rootRes != -1)
                return rootRes;
            else
                return leftRes != -1 ? leftRes : rightRes;
        }
        
        return 0;
    }
    
    TreeNode* lowestCommonAncestor(TreeNode* root, TreeNode* p, TreeNode* q) {
        traverse(root, p, q);
        
        return lca;
    }
};

int main() {
	cout << "needs driver code" << endl;
	return 0;
}
