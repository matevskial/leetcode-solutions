// https://leetcode.com/problems/best-time-to-buy-and-sell-stock-ii/

public class Solution {

	// This is O(n) solution, the idea is the same, only for every i,
	// we know what maxDiff to add to dp[i-1]
	public int maxProfit(int[] prices) {
		if (prices.length == 0) {
			return 0;
		}

		int[] dp = new int[prices.length];
		dp[0] = 0;

		int maxDiff = 0 - prices[0];
		for (int i = 1; i < prices.length; i++) {
			dp[i] = Math.max(dp[i - 1], maxDiff + prices[i]);
			maxDiff = Math.max(maxDiff, dp[i - 1] - prices[i]);
		}

		return dp[prices.length - 1];
	}

	public int maxProfitSlow(int[] prices) {
		int[] dp = new int[prices.length];
		dp[0] = 0;

		for (int i = 1; i < prices.length; i++) {
			dp[i] = dp[i - 1];
			for (int j = 0; j < i; j++) {
				int currRes = (j == 0) ? 0 : dp[j - 1];
				currRes += prices[i] - prices[j];
				dp[i] = Math.max(dp[i], currRes);
			}
		}

		return dp[prices.length - 1];
	}

	public static void main(String[] args) {
		int[] prices = new int[] { 7, 1, 5, 3, 6, 4 };
		System.out.println(new Solution().maxProfit(prices));

		prices = new int[] { 1, 2, 3, 4, 5 };
		System.out.println(new Solution().maxProfit(prices));
	}
}
