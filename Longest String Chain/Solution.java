import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

// https://leetcode.com/problems/longest-string-chain/

class Solution {
    
    // checks if s1 is predecessor of s2 according to the problem statement
    boolean isSubsequence(String s1, String s2) {
        if(s2.length() != s1.length() + 1) {
            return false;
        }
        
        int j = 0;
        for(int i = 0; i < s2.length(); i++) {
            if(s2.charAt(i) == s1.charAt(j)) {
                j++;
            }
            if(j == s1.length()) {
                return true;
            }
        }
        
        return false;
    }
    
    Map<String, Integer> memo;
    String[] words;
	Set<String> wordSet;
    
    int solveRec(String node) {
        if(memo.containsKey(node)) {
            return memo.get(node);
        }
        
        int result = 1;
        
        for(String next : words) {
            if(!node.equals(next) && isSubsequence(node, next)) {
                result = Math.max(result, solveRec(next) + 1);
            }
        }
        
        memo.put(node, result);
        return result;
    }


	// another solution where we consider only all possible words that may be chained to node
	// with this we avoid looping through words for every recursive call
    int solveRec1(String node) {
        if(memo.containsKey(node)) {
            return memo.get(node);
        }
        
        int result = 1;
        
        for(int rmIdx = 0; rmIdx < node.length(); rmIdx++) {
			String nextWord = (rmIdx == 0 ? "" : node.substring(0, rmIdx)) + node.substring(rmIdx+1, node.length());
            if(wordSet.contains(nextWord)) {
                result = Math.max(result, solveRec1(nextWord) + 1);
            }
        }
        
        memo.put(node, result);
        return result;
    }
    
    public int longestStrChain(String[] words) {

        this.words = words;
        memo = new HashMap<>();
		wordSet = new HashSet<>(Arrays.asList(words));

        int result = 0;
        for(String node : words) {
            result = Math.max(result, solveRec1(node));
        }
        
        return result;
    }

	public static void main(String[] args) {
		String[] words = new String[]{"a", "ba", "b", "bda", "bca", "bdca"};
		System.out.println(new Solution().longestStrChain(words));
	}
}
