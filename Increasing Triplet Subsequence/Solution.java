class Solution {
    
    public boolean increasingTriplet(int[] nums) {
        if(nums.length < 3)
            return false;
        
        int min = nums[0];
        Integer minCandidate = null;
        for(int i = 1; i < nums.length; i++) {
            if(nums[i] > min) {
                minCandidate = (minCandidate == null) ? nums[i] : Math.min(minCandidate, nums[i]);
            }
            
            if(minCandidate != null && nums[i] > minCandidate)
                return true;
            
            min = Math.min(min, nums[i]);
        }
        
        return false;
        
    }
}
