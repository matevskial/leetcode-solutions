/**
 * @param {number[][]} matrix
 * @return {void} Do not return anything, modify matrix in-place instead.
 */
function setZeroes(matrix) {
    for(let i = 0; i < matrix.length; i++) {
        for(let j = 0; j < matrix[i].length; j++) {
            if(matrix[i][j] == 0)
                matrix[i][j] = Infinity;
        }
    }
    
    for(let i = 0; i < matrix.length; i++) {
        for(let j = 0; j < matrix[i].length; j++) {
            if(matrix[i][j] != Infinity) continue;
            
            matrix[i][j] = 0;
            
            for(let c = 0; c < matrix[i].length; c++) {
                if(matrix[i][c] != Infinity)
                    matrix[i][c] = 0;
            }
            
            for(let r = 0; r < matrix.length; r++) {
                if(matrix[r][j] != Infinity)
                    matrix[r][j] = 0;
            }
        }
    }
}
