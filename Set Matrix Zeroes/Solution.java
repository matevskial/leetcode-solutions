class Solution {
    public void setZeroes(int[][] matrix) {
        int firstZeroRow = -1, firstZeroCol = -1;
        for(int i = 0; i < matrix.length; i++) {
            for(int j = 0; j < matrix[0].length; j++) {
                if(matrix[i][j] == 0) {
                    if(firstZeroRow == -1) {
                        firstZeroRow = i;
                        firstZeroCol = j;
                    } else {
                        matrix[i][firstZeroCol] = 0;
                        matrix[firstZeroRow][j] = 0;
                    }

                }
            }
        }
        
        if(firstZeroRow == -1)
            return;
        
        for(int i = 0; i < matrix.length; i++) {
            for(int j = 0; j < matrix[0].length; j++) {
                if(i == firstZeroRow || j == firstZeroCol)
                    continue;
                
                if(matrix[i][firstZeroCol] == 0 || matrix[firstZeroRow][j] == 0)
                    matrix[i][j] = 0;
            }
        }
        
        for(int i = 0; i < matrix.length; i++) {
            matrix[i][firstZeroCol] = 0;
        }
        
        for(int j = 0; j < matrix[0].length; j++) {
            matrix[firstZeroRow][j] = 0;
        }
    }
}
