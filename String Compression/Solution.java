// https://leetcode.com/problems/string-compression/

public class Solution {

	private int modifyInPlace(char[] chars, char c, int count, int modifyPos) {
		chars[modifyPos] = c;
		modifyPos++;

		String countStr = Integer.toString(count);
		if (count > 1) {
			for (char digit : countStr.toCharArray()) {
				chars[modifyPos] = digit;
				modifyPos++;
			}
		}

		return modifyPos;
	}

	public int compress(char[] chars) {
		int result = 0;
		int count = 0;

		int modifyPos = 0;
		for (int i = 0; i < chars.length; i++) {
			count++;

			if (i + 1 >= chars.length || chars[i] != chars[i + 1]) {
				int toAdd = 1 + ((count < 2) ? 0 : Integer.toString(count).length());
				result += toAdd;
				modifyPos = modifyInPlace(chars, chars[i], count, modifyPos);
				count = 0;
			}
		}

		for (int i = 0; i < result; i++) {
			System.out.print(chars[i] + " ");
		}
		return result;
	}

	public static void main(String[] args) {
		System.out.println(new Solution().compress(new char[] {'a', 'a', 'b', 'b', 'c', 'c', 'c'}));
	}
}
