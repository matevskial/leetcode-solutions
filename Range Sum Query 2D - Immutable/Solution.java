class NumMatrix {
    
    int[][] prefixSum;
    
    public NumMatrix(int[][] matrix) {
        
        if(matrix.length == 0) {
            prefixSum = null;
            return;
        }
        
        prefixSum = new int[matrix.length][matrix[0].length];
        
        for(int i = 0; i < matrix.length; i++) {
             for(int j = 0; j < matrix[0].length; j++) {
                 if(i == 0 && j == 0) {
                     prefixSum[i][j] = matrix[i][j];
                 } else if(i == 0) {
                     prefixSum[i][j] = prefixSum[i][j - 1] + matrix[i][j];
                 } else if(j == 0) {
                     prefixSum[i][j] = prefixSum[i - 1][j] + matrix[i][j];
                 } else {
                     prefixSum[i][j] = matrix[i][j] + prefixSum[i][j - 1] + (prefixSum[i - 1][j] - prefixSum[i - 1][j - 1]);
                 }
             }
        }
    }
    
    private int getSum(int row, int col) {
        if(row < 0 || col < 0)
            return 0;
        
        return prefixSum[row][col];
    }
    
    public int sumRegion(int row1, int col1, int row2, int col2) {
        int allSum = prefixSum[row2][col2];
        
        int aboveSum = getSum(row1 - 1, col2);
        int leftSum = getSum(row2, col1 - 1);
        int readd = getSum(row1 - 1, col1 - 1);
        
        return allSum - (aboveSum + leftSum) + readd;
    }
}

/**
 * Your NumMatrix object will be instantiated and called as such:
 * NumMatrix obj = new NumMatrix(matrix);
 * int param_1 = obj.sumRegion(row1,col1,row2,col2);
 */
