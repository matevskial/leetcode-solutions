class Solution {
    
    public int findLongestChain(int[][] pairs) {
        if(pairs.length == 0)
            return 0;
        
        Arrays.sort(pairs, (a, b) -> {
            if(a[0] == b[0])
                return a[1] - b[1];
            else
                return a[0] - b[0];
        });
        
        int result = 1;
        
        int[] dp = new int[pairs.length];
        
        dp[pairs.length - 1] = 1;
        
        for(int i = pairs.length - 2; i >= 0; i--) {
            dp[i] = 1;
            
            for(int j = pairs.length - 1; j > i; j--) {
                if(pairs[j][0] <= pairs[i][1])
                    break;
                
                dp[i] = Math.max(dp[i], dp[j] + 1);
            }
         
            
            result = Math.max(result, dp[i]);
        }
        
        return result;
    }
}
