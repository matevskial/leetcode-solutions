// https://leetcode.com/problems/best-time-to-buy-and-sell-stock/

/**
 * @param {number[]} prices
 * @return {number}
 */
var maxProfit = function(prices) {
    let minElement = prices[0];
    
    let result = 0;
    for(let i = 1; i < prices.length; i++) {
        minElement = Math.min(minElement, prices[i]);
        
        result = Math.max(result, prices[i] - minElement);
    }
    return result;
};

var main = function() {
	let prices = [7, 1, 5, 3, 6, 4];
	console.log(maxProfit(prices));
}

main()

module.exports = maxProfit;
