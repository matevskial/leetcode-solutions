class Solution {
    public int[] productExceptSelf(int[] nums) {
        int[] result = new int[nums.length];
        for(int i = nums.length - 1; i >= 0; i--) {
            if(i == nums.length - 1)
                result[i] = nums[i];
            else
                result[i] = result[i + 1] * nums[i];
        }
        
        int product = 1;
        
        for(int i = 0; i < result.length; i++) {
            int rightProduct = i == result.length - 1 ? 1 : result[i + 1];
            result[i] = product * rightProduct;
            product = product * nums[i];
        }
        
        return result;
    }
}
