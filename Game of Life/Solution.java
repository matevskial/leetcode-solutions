class Solution {
    private final int[] dirRow = {0, 0, -1, +1, -1, +1, -1, +1};
    private final int[] dirCol = {-1, +1, 0, 0, -1, -1, +1, +1};
    
    private int getLiveNeighbours(int[][] board, int r, int c) {
        int count = 0;
        for(int i = 0; i < dirRow.length; i++) {
            int rr = dirRow[i] + r;
            int cc = dirCol[i] + c;
            
            if(rr >= 0 && rr < board.length && cc >= 0 && cc < board[0].length)
                if(board[rr][cc] == 1 || board[rr][cc] == -1)
                    count++;
        }
        return count;
    }
    
    public void gameOfLife(int[][] board) {
        for(int i = 0; i < board.length; i++){
            for(int j = 0; j < board[i].length; j++) {
                int liveNeigh = getLiveNeighbours(board, i, j);

                // underpopulation
                if(board[i][j] == 1 && liveNeigh < 2)
                    board[i][j] = -1;
                
                // overpopulation
                else if(board[i][j] == 1 && liveNeigh > 3)
                    board[i][j] = -1;
                
                // reproduction
                else if(board[i][j]== 0 && liveNeigh == 3)
                    board[i][j] = 2;
            }
        }
        
        for(int i = 0; i < board.length; i++){
            for(int j = 0; j < board[i].length; j++) {
                if(board[i][j] == -1)
                    board[i][j] = 0;
                else if(board[i][j] == 2)
                    board[i][j] = 1;
            }
        }
    }
}
