// https://leetcode.com/problems/add-two-numbers/

public class Solution {
	public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
		int carry = 0;
		ListNode dummy = new ListNode(-1);
		ListNode result = dummy;

		while (l1 != null && l2 != null) {
			int sum = l1.val + l2.val + carry;
			dummy.next = new ListNode(sum % 10);
			carry = sum / 10;

			dummy = dummy.next;
			l1 = l1.next;
			l2 = l2.next;
		}

		while (l1 != null) {
			int sum = l1.val + carry;
			dummy.next = new ListNode(sum % 10);
			carry = sum / 10;

			dummy = dummy.next;
			l1 = l1.next;
		}

		while (l2 != null) {
			int sum = l2.val + carry;
			dummy.next = new ListNode(sum % 10);
			carry = sum / 10;

			dummy = dummy.next;
			l2 = l2.next;
		}

		if (carry != 0) {
			dummy.next = new ListNode(carry);
		}

		return result.next;
	}

	private static ListNode makeLinkedListFromNumber(int n) {
		ListNode dummy = new ListNode(-1);
		ListNode result = dummy;
		do {
			dummy.next = new ListNode(n % 10);
			dummy = dummy.next;
			n /= 10;
		} while (n > 0);

		return result.next;
	}

	public static void main(String[] args) {
		ListNode l1 = makeLinkedListFromNumber(89);
		ListNode l2 = makeLinkedListFromNumber(1);

		ListNode result = new Solution().addTwoNumbers(l1, l2);
		while (result != null) {
			System.out.print(result.val + " -> ");
			result = result.next;
		}
	}
}


/* Definition for singly-linked list. */
class ListNode {
	int val;
	ListNode next;

	ListNode(int x) {
		val = x;
	}
}

